/*

Задача генерацию случайного постфикса
На вход получаем значение, для которого нужно сгенерировать постфикс. Если
постфикс уже запрашивался ранее - возвращаем его.
Если нет - генерируем строку заданной длины, с заданным префиксом. В качестве
рандомных символов могут быть буквы латинского алфавита в малом регистре и цифры.
*/

function genRandomPostfix(prefix, num) {
    // 97 122
    // 48 57 // cods for prefix
    let characters = 'abcdefghijklmnopqrstuvwxyz1234567890'

    const cache = {}
        return function(value) {
            if (cache[value]) {
                return cache[value]
            } else {
                let generator = prefix
                for (let i = 0; i < num; i++) {
                    generator += randomInt(0, characters.length - 1)
                }
                cache[value] = generator
                return generator 
            }
        }
}

 randomInt = (min , max) => {
    let random = min - 0.5 + Math.random() * (max - min + 1)
     return Math.round(random)
 }

const getRandomString = genRandomPostfix('_prefix_', 4);

// _prefix_ag6t - это пример, последние 4 цифры могут быть любые
console.log(getRandomString('5689u'));

//_prefix_56po  - это пример, последние 4 цифры могут быть любые
console.log(getRandomString('1iuo'));

// _prefix_ag6t - это пример, последние 4 цифры могут быть любые, но должно
// совпадать с первым console.log
console.log(getRandomString('5689u'));