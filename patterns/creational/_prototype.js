const car = {
    wheel = 4,

    init() {
        console.log(`number of wheel ${this.wheel}, owner ${this.owner}`)
    }
}

const carWithOwner = Object.create(car, {
    owner: {
        value: 'Dmitrii'
    }
})

console.log(carWithOwner.__proto__ === car)

carWithOwner.init()

// Можно использовать как скилет какой-то обьект и он будет попадать в прототип если мы используем функцию Object.create