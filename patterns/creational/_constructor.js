// function Server(name, ip) {
//   this.name = name;
//   this.ip = ip;
// }
// Server.prototype.getUrl = function () {
//   return `https://${this.ip}:80`;
// };
// old function method

// classes
class Server {
  constructor(name, ip) {
    this.name = name;
    this.ip = ip;
  }
  getUrl() {
    return `https://${this.ip}:80`;
  }
}

const aws = new Server("AWS German", "82.21.21.32"); // this string is construcor // when we create an object with type - constructor
console.log(aws.getUrl());
