// СТРУКТУРНЫЙ ДИЗАЙН ПАТЕРН  они надо что бы уже в существующие преложение внедрить какой-то функционал не ламая прошлый .
// Адаптер позволяет интергрировать старый интерфейс какого-то класса в новый , или же что бы они работали совместно , и при этом не ломались

class OldCalc {
  operations(t1, t2, operation) {
    switch (operation) {
      case "add":
        return t1 + t2;
      case "sub":
        return t1 - t2;
      default:
        return Nan;
    }
  }
}

class NewCacl {
  add(t1, t2) {
    return t1 + t2;
  }
  sub(t1, t2) {
    return t1 - t2;
  }
}

class CaclAdapter {
  constructor() {
    this.calc = new NewCacl();
  }

  operations(t1, t2, operation) {
    switch (operation) {
      case "add":
        return this.calc.add(t1, t2);
      case "sub":
        return this.calc.add(t1, t2);
      default:
        return Nan;
    }
  }
}

const oldCalc = new OldCalc();
console.log(oldCalc.operations(10, 5, 'add')); //

const newCalc = new NewCacl();
console.log(newCalc.add(10, 5)); //

const adapter = new CaclAdapter();
console.log(adapter.operations(10, 5, "sub")); //
