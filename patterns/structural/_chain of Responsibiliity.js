// патерн который позволяет последовательно у одного и того же обьекта
// выполнять набор операций и тем самым последовательно их модифицыровать
// пример библиотека jquery

class Mysum {
    constructor(initialValue = 42) {
        this.sum = initialValue
    }
    add(value){
       this.sum += value
        return this // в этом и смысл мы должны возвращать ссылку на текущий обьект в этом паттерне

}
}

const sum1 = new Mysum()
console.log(sum1.add(8).add(10).sum) // такая цепочка и являеться дизайн патерном
const sum2 = new Mysum(0)
console.log(sum2.add(1).add(2).sum)