//позволяет савить лавушки на поля обьектов на взов функции , практический пример избавление от лишних запросов на сервер
 function networkFetch (url){
    return `${url} - ответ сервера `
 }
 const cache = new Set()
 const proxiedFetch = new Proxy(networkFetch,{
     apply(target, thisArg, args) {
         const url  = args[0]
         if (cache.has(url)) {
             return `${url} - ответ з кеша `
         } else {
             cache.add(url)
             return Reflect.apply(target, thisArg, args)
         }
     }
 })

console.log(proxiedFetch('angular.io'))
console.log(proxiedFetch('react.io'))
console.log(proxiedFetch('angular.io'))