/*
Прислал Дмитрий Жуков.

В функцию mostFrequentDays передается год (целое число), необходимо реализовать
функцию так, чтобы из нее вернулся массив с наиболее часто встречаемыми днями
недели в году, что был передан. Массив должен быть отсортирован по дням недели
(от понедельника к воскресенью).
*/

function mostFrequentDays(year) {
 let day = new Date(year , 0).toLocaleString("ru" , {weekday : "long"});
    let isDay = new Date(year , 1,29).getMonth() === 1;
    if (isDay) {
        return [day]
    } else {
        let nextDay = new Date(year , 0, 2).toLocaleString("ru" , {weekday : "long"});
         return [day , nextDay]
    }

}

console.log(mostFrequentDays(2016)); // ["пятница"]
console.log(mostFrequentDays(2019)); // ["вторник", "среда"]
console.log(mostFrequentDays(2020)); // ["среда"]


// function mostFrequentDays(year) {
//     return new Date(year, 1, 29).getMonth() === 1
//         ? [new Date(year, 0, 2), new Date(year, 0, 1)]
//             .sort((a, b) => (a.getDay() === 0 ? 7 : a.getDay()) - (b.getDay() === 0 ? 7 : b.getDay()))
//             .map(val => val.toLocaleString('ru', {weekday: 'long'}))
//         : [new Date(year, 0, 1).toLocaleString('ru', {weekday: 'long'})];
// }