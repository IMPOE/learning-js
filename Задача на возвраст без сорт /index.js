/*

Подкину задачку, которую сам решал) Дан массив возрастов в семье, вернуть
массив c возрастом самого младшего, самого старшего и разницу между самым
старшим и младшим. Если ребенку 9 месяцев, считаем как 0. P.S. Желательно
не использовать встроенные функции типа sort и т.п.
*/

function differenceInAges(ages) {
let minAge , maxAge;
minAge = maxAge = ages[0]
for (let i = 1 ; i < ages .length; i++ ) {
    if(ages[i] < minAge) minAge = ages[i]
    if(ages[i] > maxAge) maxAge = ages[i]
}
return [minAge,maxAge,maxAge - minAge]
}

console.log(differenceInAges([82, 15, 6, 38, 35])); // [6, 82, 76]
console.log(differenceInAges([57, 99, 14, 32])); // [14, 99, 85]