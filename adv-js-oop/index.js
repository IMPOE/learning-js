class TextControll {
  constructor(minLength, textElem) {
    this.validateRules = {
      minLength: minLength,
    };
    this.wrapper = document.createElement('div');
    this.label = document.createElement('label');
    this.textElem = textElem; // просто принимает какойто елеменит (инпут , текст ареа)
  }

  getValue() {
    this.textElem.value;
  }

  setValue(value) {
    const val = this.textElem.value;
    if (val.length < this.validateRules.minLength) {
      this.setError('invalid min length');
      throw new Error('min length short');
    }
  }

  setError(errorMassage) {
    this.label.innerText = errorMassage;
    this.label.style.color = 'red';
  }

  render(parent) {
    this.wrapper.append(this.label, this.textElem);
    parent.append(this.wrapper);
  }
}

class textArea extends TextControll {
  // вызываеться конструктор текст ареа  который вызывает общий конструктор
  // в текст контрол с помощью ключевого слова супер
  constructor(minLength, rows) {
    const textarea = document.createElement('textarea');
    textarea.setAttribute('rows', rows);

    super(minLength, textarea); // устанавливаем текст елемент
    // с помощью аргумента
    // в конструкторе мы вызываем конструктор родительского класса в нем передаем аргумент
    // из текст ареа и второй аргумент  просто генерируем
    this.clearButton = textElem.createButton(this); // генерируем новый елемент
  }

  static createButton(textarea) {
    //  навешиваем обрабочик событий
    const clearButton = document.createElement('button');
    clearButton.innerText = 'clear';
    clearButton.addEventListener('click', () => {
      textarea.setValue('');
    });

    return clearButton;
  }

  render(parent) {
    super.render(parent); // в ментоде рендер вызываем метод  базового класса родителя

    this.wrapper.append(this.clearButton); // добавляем нашу кнопку до текст контрол рендера
  }
}

class Input extends TextControll {
  constructor(minLength) {
    // принимаем минимальную длинну
    super(minLength, document.createElement('input')); // передаем в метод супер
  }
}

class LiveValidationInput extends Input {
  constructor(minLength) {
    super(minLength);

    this.textElem.addEventListener('input', () => {
      this.onChange(e.target.value);
    });
  }

  onChange(value) {
    if (value.length) {
      this.textElem.style.backgroundColor = 'green';
    } else {
      this.textElem.style.backgroundColor = 'red';
    }
  }
}

const formContent = document.getElementById('form');
const descriptiontextElem = new textElem(3, 10); // наш текст ериа
descriptiontextElem.render(formContent); // мы его создаем и вызываем в нутрь елемента форм контент

descriptiontextElem.getValue();
descriptiontextElem.setValue('fetch');

const input = new LiveValidationInput();
input.render(formContent);
input.getValue();

// HANDLER SUBMITE
function submitHandler() {
  // other fields ...
  try {
    const description = descriptiontextElem.getValue();
  } catch (e) {
    //print error
  }
}
