// асинхронность с помощью колбеков
// плохо в том что получаем большую вложенность
console.log('request data ..');

// setTimeout(() => {
//   console.log('preparing data..');

//   const backendData = {
//     server: 'aws',
//     port: 2000,
//     status: 'working',
//   };

//   setTimeout(() => {
//     backendData.modified = true;
//     console.log('data received', backendData);
//   }, 2000);
// }, 2000);
//  setTimeout асинхронная функция которая поступает из браузерного API

// c колбеками при ошибке мы должны писать кучу ифов для проверки статусов

// Промисы упрощают работу с асинхронными операциями

//1. создать новую переменну и добавить клас проммис

// это клас в конструктор которого мы должны пережать колбэк функцию
//данная функция принимает в себя 2 аргумента первый аргумент resolve второй reject
// и эти 2 параметра так же функции

// const p = new Promise(function (resolve, reject) {
//   setTimeout(() => {
//     console.log('preparing data..');

//     const backendData = {
//       server: 'aws',
//       port: 2000,
//       status: 'working',
//     };
//     resolve(backendData);
//   }, 2000);
// });

// p.then((data) => {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       data.modified = true;
//       resolve(data);
//     }, 2000);
//   });
// })
//   .then((clientData) => {
//     clientData.fromPromise = true;

//     return clientData;
//   })
//   .then((data) => {
//     console.log('modified', data);
//   })
//   .catch((err) => console.error('Error', err))
//   .finally(() => {
//     console.log('finnaly');
//   });
// несколько зенов подряд это чейн

const sleep = (ms) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, ms);
  });
};

// sleep(2000).then(() => console.log('after 2 second '));
// sleep(5000).then(() => console.log('after 3 second '));

Promise.all([sleep(2000), sleep(3000)]).then(() => {
  console.log('all promises');
});

Promise.race([sleep(2000), sleep(3000)]).then(() => {
  console.log('Race promises');
});
// промисы это определенная обертка над асинхронностью которая добавляет удобства для написания кода
