
$(function(){
 const  useFakeData = false
  function start() {
   initMonthClick()
   // getCurrentMonthEvents()
   showSelectedMonthMessage(true)
  }

  function showSelectedMonthMessage(show){
   const massage = $(".message__select_month");
   show ?  massage.show() : massage.hide();
  }

  function initMonthClick() {
  $(".month").on("click",function(){
   const monthId = $(this).data("id")
   console.log(monthId)
   showEventsForMonth(monthId)
  })
  }

  function getCurrentMonthEvents() {
   const  currentDate = new Date()
   const currentMonth = currentDate.getMonth()
   showEventsForMonth(currentMonth)
  }

 function showEventsForMonth(monthId) {
  showSelectedMonthMessage(false)
 useFakeData ? getFakeEventsByMonth(monthId,drawEvents):
     getEventsByMonth(monthId,drawEvents)

 }

 function getFakeEventsByMonth(monthId ,callback) {
  const data = [
   {
    activity: 'Drink Coffee',
    id : 1,
    dateTime: `01.${monthId}.2020 12:00`,

   }
  ]
  callback(monthId, data)
 }

 function getEventsByMonth(monthId , callback) {
  $.get("calendardata.json",function(data){
   const filtered = data.filter(function(el){
    return (el.id - 1) == monthId
   })
   callback(monthId,filtered)
  })
 }

 function drawEvents(monthId, events){
  selectCurrentMonth(monthId)
  renderEvents(events)
 }

 function selectCurrentMonth(monthId){
 $(".month").removeClass("active");
  $(`.month[data-id="${monthId}"]`).addClass("active")
 }

 function renderEvents(events){
 const  massageNoEvents = $('.massage__no_events')
  const list = $(".events__list")
  list.empty()

  if (events.length > 0 ){
   massageNoEvents.hide()

 const items = events.map(function(el) {
  return `<li>${el.dateTime} - ${el.activity}</li>`
 }).join('')
   let result = `<ul> ${items} </ul>`
   list.append(result)
  }else {
   massageNoEvents.show()
  }
 }

  start()
})