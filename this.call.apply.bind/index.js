const persone = {
  name: 'Roma',
  say: function (what) {
    console.log(`${this.name} say ${what}`);
  },
};

const Ura = { name: 'Ura' };

persone.say.call(Ura, 'Bye');
persone.say.apply(Ura, ['Bye']); // second arg array in apply
persone.say.call(Ura, ...['Bye']); // or use  spred

persone.say.bind(Ura, ['Bye']); // first parametr contecst , second infin arg..
// отличие байнд он не вызывает функцию сразу же а возвращает новую функцию
// потому либо мы ее сразу же можем вызвать persone.say.bind(Ura,['Bye'])()

// ли бо же можем записать в переменную и вызвать потом  const bound = persone.say.bind(Ura,['Bye']); bound()

persone.say('Good Bye');

//=================================================//

function Person(name, age) {
  (this.name = name), (this.age = age);

  console.log(this);
}

const Roma = new Person('Roma', 25);
// ===============================================//

function logThis() {
  console.log(this);
}

const obj = { num: 42 };
logThis.apply(obj);
logThis.call(obj);
logThis.bind(obj)();

//=================================================//
//неявная привязка контекста //
const animal = {
  lags: 4,
  logThis: function () {
    console.log(this);
  },
};

animal.logThis();

//===================================================//
// arrow function this

function Cat(color) {
  this.color = color;

  console.log('this', this);
  (() => {
    console.log('arrow this', this);
  })();
}

new Cat('red');
