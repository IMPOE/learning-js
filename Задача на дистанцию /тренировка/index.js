/*
Прислал Serega Mangushev.
В функцию realizeDistance передается дистанция, которую должен проехать человек,
вторым параметром передается дистанция после которой необходимо сделать
остановку. Необходимо вывести лог всего пути внутри функции, как в примере.
*/

function realizeDistance(distance, stopAfter) {
let counter = 0;
let currentDistance = distance;
let str = "";

    while (distance > 0 && currentDistance > stopAfter) {
        counter++;
        currentDistance -= stopAfter;
        str += `Остановка №${counter}. Вы проехали ${stopAfter * counter} метров.\n`

    }

    str += counter === 0
    ? ` Вы проехали ${distance} метров и доехали до точки.`
        : `Вы проехали еще ${distance - stopAfter * counter} метров и доехали до точки.`

    return str

}

// Вы проехали 100 метров и доехали до точки.
console.log(realizeDistance(100, 150));

/*
Остановка №1. Вы проехали 300 метров.
Остановка №2. Вы проехали 600 метров.
Остановка №3. Вы проехали 900 метров.
Вы проехали еще 100 метров и доехали до точки.
*/
console.log(realizeDistance(1000, 300));