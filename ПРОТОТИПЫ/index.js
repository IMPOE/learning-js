// каждый обьект имеет свой протоип который береться от родител елем
// у которого был создан данный обьект

//===================================================================//
// __proto__ (ES6)получение прототипа родительского елем ,указывает на проттотип родительского класа иили обьекта от которого был создан данный обьект
// в джава скрипте любое наследое идет как прототипированое (по цепочке - дереву)//
// ES5 было свойвство Object.getTprototypeOf()

//2===================================================================//
// свойтво прототайп у различных функций - который служит что бы передавать эти свойства для обьектов которые создаються  например через ключевое слово new
//2
function Programmer(name, program) {
  (this.name = name), (this.program = program);
}

Programmer.prototype.print = function () {
  console.log(`My favorite library ${this.program}`);
};

const roma = new Programmer('Roma', 'React');
roma.print();

console.log(Programmer.prototype);
console.log(roma.__proto__);
console.log(Programmer.prototype == roma.__proto__);
console.log(Programmer.prototype === roma.__proto__);
console.log(roma.constructor); // конструктор указывает на конструкторский родительський клас
console.log(roma);
// смотреть на инстанс
// вызываем в контексте обьекта рома темод принт он его не находит в самом обьекте поэтому он спускаетсья в прототип и там есть метод войс

//=================================================================//
//---Свойства обьектов или свойства которые доступны в прототипе---//
// --------------------- Object.create ----------------------------//
let proto = { year: 2019 };
const myYear = Object.create(proto);
// первым параметром передаем обьект который будет являться прототипом для даного обьекта , вторым параметром можно передавать дополнительные свойства
console.log(myYear.year);
console.log(myYear.hasOwnProperty('year'));
console.log(myYear.__proto__ === proto);

// Данный обьект считаеться прототипом так как мы использовали функцию Object.create
proto.year = 2020;
console.log(myYear.year);

proto = { year: 999 }; // myYear.year не поменяеться  на 999 останеться 2020
console.log(myYear.year);
