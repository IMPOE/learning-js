function Cat(color, name) {
  (this.name = name), (this.color = color);
}

// const cat = new Cat('black', 'Roma');
// console.log(cat);

//--------------------------------------------//
//now do your own conctructor new

function MyNew(constructor, ...args) {
  const obj = {};
  Object.setPrototypeOf(obj, constructor.prototype);
  return constructor.apply(obj, args) || obj;
}

const catTwo = MyNew(Cat, 'black', 'Roma');
console.log(catTwo);
