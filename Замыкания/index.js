// function createManager() {
//   const fw = ['React', 'Vue'];

//   return {
//     print: function () {
//       console.log(fw.join(' '));
//     },
//     add: function (frameWork) {
//       fw.push(frameWork);
//     },
//   };
// }

// const manager = createManager();
// manager.print();
// manager.add('Angular');
// manager.add('Go');
// manager.print();

const fib = [1, 2, 3, 5, 8, 13];

for (var i = 0; i < fib.length; i++) {
  // or let
  (function (j) {
    setTimeout(() => {
      console.log(`fib[${j}] = ${fib[j]}`);
    });
  })(i);
}
